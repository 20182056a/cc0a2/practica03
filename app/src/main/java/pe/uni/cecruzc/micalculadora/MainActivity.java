package pe.uni.cecruzc.micalculadora;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    RadioButton rbBasicMain, rbScientificMain, rbProgrammerMain;
    Button goButtonMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rbBasicMain = findViewById(R.id.rb_basic_main);
        rbScientificMain = findViewById(R.id.rb_scientific_main);
        rbProgrammerMain = findViewById(R.id.rb_programmer_main);
        goButtonMain = findViewById(R.id.btn_go_main);

        goButtonMain.setOnClickListener(v -> {
            if(!rbBasicMain.isChecked() && !rbScientificMain.isChecked() && !rbProgrammerMain.isChecked()) {
                Toast.makeText(MainActivity.this, R.string.main_no_option_selected_msg, Toast.LENGTH_LONG).show();
                return;
            }

            if(rbBasicMain.isChecked()) {
                Intent intent = new Intent(MainActivity.this, CalculatorActivity.class);
                intent.putExtra("Mode", "Basic");
                startActivity(intent);
            }
            if(rbScientificMain.isChecked() || rbProgrammerMain.isChecked()) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder
                        .setTitle(R.string.main_title_dialog)
                        .setMessage(R.string.main_msg_dialog)
                        .setCancelable(false)
                        .setPositiveButton(R.string.main_positive_dialog, (dialog, which) -> {

                        })
                        .setNegativeButton(R.string.main_negative_dialog, (dialog, which) -> {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        })
                        .create()
                        .show();
            }
        });
    }
}
package pe.uni.cecruzc.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    TextView tvTitleSplash;
    ImageView ivLogoSplash;

    Animation animationLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        tvTitleSplash = findViewById(R.id.tv_title_splash);
        ivLogoSplash = findViewById(R.id.iv_logo_splash);

        animationLogo = AnimationUtils.loadAnimation(this, R.anim.logo_animation);

        ivLogoSplash.setAnimation(animationLogo);

        new CountDownTimer(6000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();

    }
}
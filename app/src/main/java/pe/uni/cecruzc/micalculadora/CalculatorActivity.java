package pe.uni.cecruzc.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CalculatorActivity extends AppCompatActivity {

    TextView tvMsg;
    Button btnReturn, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btnDel, btnCalculate, btnPlus, btnMinus, btnTimes, btnDivision;
    int result = 0;
    Boolean isPlusSelected, isMinusSelected, isTimesSelected, isDivisionSelected;
    int accumulate = 0, input = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        tvMsg = findViewById(R.id.tv_msg_calulator);
        btn1 = findViewById(R.id.btn_1_calculator);
        btn2 = findViewById(R.id.btn_2_calculator);
        btn3 = findViewById(R.id.btn_3_calculator);
        btn4 = findViewById(R.id.btn_4_calculator);
        btn5 = findViewById(R.id.btn_5_calculator);
        btn6 = findViewById(R.id.btn_6_calculator);
        btn7 = findViewById(R.id.btn_7_calculator);
        btn8 = findViewById(R.id.btn_8_calculator);
        btn9 = findViewById(R.id.btn_9_calculator);
        btn0 = findViewById(R.id.btn_0_calculator);
        btnDel = findViewById(R.id.btn_delete_calculator);
        btnCalculate = findViewById(R.id.btn_calculate_calculator);
        btnPlus = findViewById(R.id.btn_plus_calculator);
        btnMinus = findViewById(R.id.btn_minus_calculator);
        btnTimes = findViewById(R.id.btn_times_calculator);
        btnDivision = findViewById(R.id.btn_division_calculator);
        btnReturn = findViewById(R.id.btn_return_main_calculator);

        btnCalculate.setOnClickListener(v -> {
            result = 0;

            isPlusSelected = false;
            isMinusSelected = false;
            isTimesSelected = false;
            isDivisionSelected = false;
        });

        btnPlus.setOnClickListener(v -> {
            accumulate += input;
            isPlusSelected = true;
            input = 0;
            tvMsg.setText(String.valueOf(input));
        });

        btnDel.setOnClickListener(v -> {
            input /= 10;
            tvMsg.setText(String.valueOf(input));
        });

        btn0.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            tvMsg.setText(String.valueOf(input));
        });

        btn1.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 1;
            tvMsg.setText(String.valueOf(input));
        });

        btn2.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 2;
            tvMsg.setText(String.valueOf(input));
        });

        btn3.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 3;
            tvMsg.setText(String.valueOf(input));
        });

        btn4.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 4;
            tvMsg.setText(String.valueOf(input));
        });

        btn5.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 5;
            tvMsg.setText(String.valueOf(input));
        });

        btn6.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 6;
            tvMsg.setText(String.valueOf(input));
        });

        btn7.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 7;
            tvMsg.setText(String.valueOf(input));
        });

        btn8.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 8;
            tvMsg.setText(String.valueOf(input));
        });

        btn9.setOnClickListener(v -> {
            if(String.valueOf(input).length() == 9) return;
            input *= 10;
            input += 9;
            tvMsg.setText(String.valueOf(input));
        });

        btnReturn.setOnClickListener(v -> finish());

    }
}